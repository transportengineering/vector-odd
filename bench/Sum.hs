-- -*- dante-target: "sum" -*-
{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}
import           Data.Foldable
import           Data.Text             (Text)
import qualified Data.Text             as T
import qualified Data.Vector           as V
import qualified Data.Vector.Mutable   as M
import qualified Data.Vector.Odd.Slice as S
import           Gauge

finalise :: Foldable f => f Text -> Int
finalise = foldl' (\s t -> T.length t + s) 0

{-# INLINE mkbench #-}
mkbench :: (Foldable f, a ~ Text) => (Int -> a -> f a -> f a) -> (v -> f a) -> v -> Int
mkbench set from =
  finalise
  . set 49 "what"
  . set 499 "what"
  . set 43 "what"
  . set 400 "what"
  . set 488 "aaa"
  . set 500 "bbb"
  . set 502 "ccc"
  . set 503 "ddd"
  . set 504 "eee"
  . set 300 "aaa"
  . set 200 "bbb"
  . set 150 "ccc"
  . set 160 "ddd"
  . set 170 "eee"
  . set 180 "aaa"
  . set 180 "bbb"
  . set 190 "ccc"
  . set 600 "ddd"
  . set 700 "eee"
  . set 750 "aaa"
  . set 751 "bbb"
  . set 800 "ccc"
  . set 900 "ddd"
  . set 910 "eee"
  . from

slice :: V.Vector Text -> Int
slice = mkbench S.set S.fromVector

vector :: V.Vector Text -> Int
vector = mkbench set id
  where set i a v = V.modify (\m -> M.write m i a) v

main :: IO ()
main = defaultMain
  [ env (return (V.replicate 1000 ("test" :: Text))) $ \ ~vec ->
      bgroup "sums"
      [ bench "slice" (nf slice vec)
      , bench "vector" (nf vector vec)
      ]
  ]
