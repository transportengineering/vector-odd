{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE MagicHash           #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
module Data.Vector.Odd.Index
  ( Odd
  , optimise
    -- * Construction
  , fromVector
  , fromList
  , empty
  , singleton
  , append
  , enumFromTo
    -- * Updates
  , set
  , modify
    -- * Deconstruction
  , toVector
  , toList
    -- * Indexing
  , (!)
  , (!?)
    -- * Size
  , length
  , depth
    -- * Folds
  , foldr
  , foldr'
  , foldl
  , foldl'
    -- ** Indexed variants
  , ifoldr
  , ifoldr'
  , ifoldl
  , ifoldl'
    -- * Traversals
  , mapM
  , mapM_
    -- ** Indexed variants
  , imapM
  , imapM_
  ) where
import           Data.Functor.Const
import           Data.Monoid
import           Data.Proxy
import qualified Data.Vector                 as V
import qualified Data.Vector.Generic         as G
import qualified Data.Vector.Generic.Mutable as GM
import qualified Data.Vector.Storable        as S
import qualified Data.Vector.Unboxed         as U
import           GHC.Int                     (Int (..))
import           GHC.Prim                    (Int#, tagToEnum#, (+#), (-#),
                                              (<#), (<=#), (==#), (>#))
import           Prelude                     hiding (enumFromTo, foldl, foldl',
                                              foldr, length, map, mapM, mapM_,
                                              replicate)

import           Data.Vector.Odd.Internal

-- | An index-based data structure. Conceptually it is just a @ Int -> a @. This
-- should have faster indexing, but slower everything-else.
--
-- It is possible to implement many instances for this type, such as 'Monad',
-- 'Functor', 'Eq', 'Ord', etc. -- but to a tee they would all use 'toVector' in
-- a way that would duplicate work if the user ever used the 'Odd' again. If you
-- need these instances, use 'toVector' first, and transform that back into an
-- 'Odd' with 'fromVector'.
data Odd a = Odd
  { index#  :: Int# -> a
  , length# :: Int#
  , depth#  :: Int# -- ^ maximum number of indirections
  }

instance Show a => Show (Odd a) where
  show = show . toVector @V.Vector

instance Semigroup (Odd a) where
  (<>) = append

instance Monoid (Odd a) where
  mempty = empty

{-# INLINE optimise #-}
-- | /O(N * D)/
optimise :: forall v a. G.Vector v a => Odd a -> Odd a
optimise = fromVector @v . toVector

--------------------------------------------------------------------------------
-- Construction

-- | /O(1)/
fromVector :: G.Vector v a => v a -> Odd a
fromVector !vec = Odd
  { length# = case G.length vec of I# a -> a
  , depth# = 1#
  , index# = \i -> G.unsafeIndex vec (I# i)
  }

fromList :: [a] -> Odd a
fromList = fromVector . V.fromList

empty :: Odd a
empty = Odd
  { index# = outOfBoundsError# "empty" 0#
  , length# = 0#
  , depth# = 0#
  }

singleton :: a -> Odd a
singleton a = Odd
  { index# = \i -> case i of
      0# -> a
      _  -> outOfBoundsError# "singleton" 1# i
  , length# = 1#
  , depth# = 1#
  }

-- | /O(1)/
append :: Odd a -> Odd a -> Odd a
append a b = Odd
  { length# = length# a +# length# b
  , depth# = 1# +# max# (depth# a) (depth# b)
  , index# = \i -> case i <# length# a of
      1# -> index# a i
      _  -> index# b (i -# length# a)
  }

-- | /O(1)/
enumFromTo :: Int -> Int -> Odd Int
enumFromTo (I# a) (I# b) = Odd
  { length# = (b -# a) +# 1#
  , depth# = 1#
  , index# = \i -> I# (a +# i)
  }

-- | /O(1)/
replicate :: Int -> a -> Odd a
replicate (I# l) a = Odd
  { length# = l
  , depth# = 1#
  , index# = \_ -> a
  }

--------------------------------------------------------------------------------
-- Updates

{-# INLINE set #-}
-- | /O(N)/ Beware
set :: Odd a -> Int -> a -> Odd a
set Odd{index#, length#, depth#} (I# i) a = Odd
  { depth# = depth# +# 1#
  , length# = length#
  , index# = \j -> case j ==# i of
      1# -> a
      _  -> index# j
  }

{-# INLINE modify #-}
-- | /O(N)/ Beware
modify :: Odd a -> Int -> (a -> a) -> Odd a
modify Odd{index#, depth#, length#} (I# i) f = Odd
  { depth# = depth# +# 1#
  , length# = length#
  , index# = \j -> case j ==# i of
      1# -> f (index# j)
      _  -> index# j
  }

--------------------------------------------------------------------------------
-- Deconstruction

-- | /O(N * D)/
toVector :: forall v a. G.Vector v a => Odd a -> v a
toVector = mapToVector id

toList :: Odd a -> [a]
toList = foldr (:) []

--------------------------------------------------------------------------------
-- Size

length :: Odd a -> Int
length Odd{length#} = I# length#

depth :: Odd a -> Int
depth Odd{depth#} = I# depth#

--------------------------------------------------------------------------------
-- Internal utility

{-# INLINE slice# #-}
slice# :: Odd a
       -> (Int# -> a -> b -> b)
       -> b
       -> Int# -- ^ starting index (inclusive)
       -> Int# -- ^ end index (non-inclusive)
       -> b
slice# Odd{index#, length#} f z0 start end = go start
  where
    go i = case i <# end of
      1# -> f i (index# i) (go (i +# 1#))
      _  -> z0

{-# INLINE mapToVector #-}
mapToVector :: forall v b a. G.Vector v b => (a -> b) -> Odd a -> v b
mapToVector f o@Odd{length#} = G.create (do
  m <- GM.new (I# length#)
  slice# o
    (\i# a acc -> acc >> GM.unsafeWrite m (I# i#) (f a))
    (return ())
    0#
    length#
  return m)

--------------------------------------------------------------------------------
-- Indexing

{-# INLINE (!) #-}
-- | /O(D)/
(!) :: Odd a -> Int -> a
(!) Odd{index#, length#} (I# i#)
  | tagToEnum# (i# <# length#) = index# i#
  | otherwise                  = outOfBoundsError# "(!)" i# length#

{-# INLINE (!?) #-}
-- | /O(D)/
(!?) :: Odd a -> Int -> Maybe a
(!?) Odd{length#, index#} (I# i#)
  | tagToEnum# (i# <# length#) = Just (index# i#)
  | otherwise                  = Nothing

--------------------------------------------------------------------------------
-- Maps

{-# INLINE map #-}
-- | /O(1)/
map :: forall v a b. G.Vector v b => (a -> b) -> Odd a -> Odd b
map f Odd{length#, depth#, index#} = Odd
  { length#
  , depth# = 1# +# depth#
  , index# = \i -> f (index# i)
  }

--------------------------------------------------------------------------------
-- Folds

{-# INLINE foldr #-}
{-# INLINE ifoldr #-}
{-# INLINE foldl #-}
{-# INLINE ifoldl #-}
{-# INLINE foldl' #-}
{-# INLINE ifoldl' #-}

-- | Right-associative fold
foldr :: (a -> b -> b) -> b -> Odd a -> b
foldr f z o@Odd{length#} = slice# o (\_ !x xs -> f x xs) z 0# length#

-- | Right-associative strict fold
foldr' :: (a -> b -> b) -> b -> Odd a -> b
foldr' f z o@Odd{length#} = slice# o (\_ !x !xs -> f x xs) z 0# length#

-- | Right-associative strict fold, with index
ifoldr :: (Int -> a -> b -> b) -> b -> Odd a -> b
ifoldr f z o@Odd{length#} = slice# o (\i !x xs -> f (I# i) x xs) z 0# length#

-- | Right-associative strict fold, with index
ifoldr' :: (Int -> a -> b -> b) -> b -> Odd a -> b
ifoldr' f z o@Odd{length#} = slice# o (\i !x !xs -> f (I# i) x xs) z 0# length#

-- | Left-associative fold
foldl :: (b -> a -> b) -> b -> Odd a -> b
foldl f z0 o@Odd{length#} = slice# o (\_ !x k z -> k (f z x)) id 0# length# z0

-- | Strict left-associative fold
foldl' :: (b -> a -> b) -> b -> Odd a -> b
foldl' f z0 o@Odd{length#} = slice# o (\_ !x k z -> k $! f z x) id 0# length# z0

-- | Left-associative fold, with index
ifoldl :: (Int -> b -> a -> b) -> b -> Odd a -> b
ifoldl f z0 o@Odd{length#} = slice# o (\i !x k z -> k (f (I# i) z x)) id 0# length# z0

-- | Strict left-associative fold, with index
ifoldl' :: (Int -> b -> a -> b) -> b -> Odd a -> b
ifoldl' f z0 o@Odd{length#} = slice# o (\i !x k z -> k $! f (I# i) z x) id 0# length# z0

--------------------------------------------------------------------------------
-- Traversals

{-# INLINE mapM #-}
{-# INLINE imapM #-}
{-# INLINE mapM_ #-}
{-# INLINE imapM_ #-}

-- | /O(N * D)/ optimising
mapM :: Monad m => (a -> m b) -> Odd a -> m (Odd b)
mapM f = fmap fromVector . V.mapM f . toVector

-- | /O(N * D)/ optimising
imapM :: Monad m => (Int -> a -> m b) -> Odd a -> m (Odd b)
imapM f = fmap fromVector . V.imapM f . toVector

-- | /O(N * D)/
mapM_ :: Monad m => (a -> m ()) -> Odd a -> m ()
mapM_ f Odd{index#, length#} = go 0#
  where
    go i = case i <# length# of
      1# -> f (index# i) >> go (i +# 1#)
      _  -> return ()

-- | /O(N * D)/
imapM_ :: Monad m => (Int -> a -> m ()) -> Odd a -> m ()
imapM_ f Odd{index#, length#} = go 0#
  where
    go i = case i <# length# of
      1# -> f (I# i) (index# i) >> go (i +# 1#)
      _  -> return ()

