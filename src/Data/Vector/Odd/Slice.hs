{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE BangPatterns        #-}
{-# LANGUAGE MagicHash           #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
module Data.Vector.Odd.Slice
  ( Odd
  , optimise
    -- * Construction
  , fromVector
  , fromList
  , empty
  , singleton
  , append
    -- * Slice
  , sliceFromTo
  , slice
    -- * Deconstruction
  , toVector
  , toList
    -- * Updates
  , set
  , sets
    -- * Indexing
  , (!)
  , (!?)
    -- * Size
  , length
  , depth
    -- * Folds
  , foldr
  , foldr'
  , foldl
  , foldl'
    -- ** Indexed variants
  , ifoldr
  , ifoldr'
  , ifoldl
  , ifoldl'
    -- * Traversals
  , mapM
  , mapM_
    -- ** Indexed variants
  , imapM
  , imapM_
  ) where
import qualified Data.Foldable               as F
import           Data.Functor.Const
import           Data.Monoid
import           Data.Proxy
import qualified Data.Vector                 as V
import qualified Data.Vector.Generic         as G
import qualified Data.Vector.Generic.Mutable as GM
import qualified Data.Vector.Storable        as S
import qualified Data.Vector.Unboxed         as U
import           GHC.Int                     (Int (..))
import           GHC.Prim                    (Int#, tagToEnum#, (+#), (-#),
                                              (<#), (<=#), (==#), (>#))
import           Prelude                     hiding (foldl, foldl', foldr,
                                              length, map, mapM, mapM_,
                                              replicate)

import           Data.Vector.Odd.Internal

--------------------------------------------------------------------------------

-- | A data structure based on folds of slices of vectors. This should have
-- faster iteration and folds than the index-based structure in
-- "Data.Vector.Odd.Index", but slower indexing.
--
-- It is possible to implement many instances for this type, such as 'Monad',
-- 'Functor', 'Eq', 'Ord', etc. -- but to a tee they would all use 'toVector' in
-- a way that would duplicate work if the user ever used the 'Odd' again. If you
-- need these instances, use 'toVector' first, and transform that back into an
-- 'Odd' with 'fromVector'.
data Odd a = Odd
  { slice#
    :: forall b.
       FoldType
    -> (Int# -> a -> b -> b)
    -> b
    -> Int#
    -> Int#
    -> b
  , length# :: Int#
  , depth# :: Int# -- ^ maximum number of indirections
  }

instance Show a => Show (Odd a) where
  show = show . toVector @B

instance Semigroup (Odd a) where
  (<>) = append

instance Monoid (Odd a) where
  mempty = empty

{-# INLINE optimise #-}
-- | /O(N * D)/
optimise :: forall v a. G.Vector v a => Odd a -> Odd a
optimise = fromVector @v . toVector

--------------------------------------------------------------------------------
-- Construction

-- | /O(1)/

{-# INLINE fromVector #-}
fromVector :: G.Vector v a => v a -> Odd a
fromVector !vec = Odd
  { length# = case G.length vec of I# a -> a
  , depth# = 1#
  , slice# = isfoldr# vec
  }

fromList :: [a] -> Odd a
fromList = fromVector . V.fromList

{-# INLINE empty #-}
empty :: Odd a
empty = Odd{slice# = \_ _ z _ _ -> z, length# = 0#, depth# = 0#}

{-# INLINE singleton #-}
singleton :: a -> Odd a
singleton a = Odd
  { slice# = \_ f z _ _ -> f 0# a z
  , length# = 1#
  , depth# = 1#
  }

-- | /O(1)/
{-# INLINE append #-}
append :: Odd a -> Odd a -> Odd a
append a b = Odd
  { length# = length# a +# length# b
  , depth# = 1# +# max# (depth# a) (depth# b)
  , slice# = \m f z i l ->
      slice# a
      m
      f
      (slice# b
       m
       (\i' x acc -> f (i' +# length# a) x acc)
       z
       (i -# length# a)
       (l -# length# a))
      i
      (length# a)
  }

--------------------------------------------------------------------------------
-- Updates

{-# INLINE set #-}
-- | /O(1)/
set :: Int -> a -> Odd a -> Odd a
set (I# i) a Odd{slice#, length#, depth#} = Odd
  { depth# = depth# +# 1#
  , length# = length#
  , slice# = \m f z from to -> case from <=# i of
      1# -> case i <# to of
        1# ->
          -- nodes given from <= i < to
          -- 1. [from, i)
          -- 2. [i, i+1)
          -- 3. [i+1, to)
          slice# -- 1
          m
          f
          (-- 2
          f i a
          (slice# -- 3
           m
           f
           z
           (i +# 1#)
           to))
          from
          i
        _ -> slice# m f z from to -- from < i && i >= to
      _ -> slice# m f z from to -- from > i
  }

{-# INLINE sets #-}
-- | /O(1)/
sets :: (Int, Odd a) -> Odd a -> Odd a
sets (!start, !v) odd
  | start + length v < length odd
  = sliceFromTo 0 start odd
    <> v
    <> sliceFromTo (start + length v) (length odd) odd

  | otherwise =
    error "Data.Vector.Odd.Slice.sets: vector must be within existing Odd"

{-# INLINE sliceFromTo #-}
sliceFromTo :: Int -> Int -> Odd a -> Odd a
sliceFromTo (I# from) (I# to) Odd{slice#, length#, depth#}
  | tagToEnum# (from <# length#) && tagToEnum# (to <=# length#) = Odd
    { length# = to -# from
    , depth# = depth# +# 1#
    , slice# = \m f z0 from' to' ->
        -- map from' and to' to previous indices
        slice#
        m
        (\i a z -> f (i -# from) a z)
        z0
        from
        to
    }
  | otherwise =
    error $ "invalid slice " ++ show (I# from, I# to, I# length#)

{-# INLINE slice #-}
slice :: Int -> Int -> Odd a -> Odd a
slice from len = sliceFromTo from (from+len)

--------------------------------------------------------------------------------
-- Deconstruction

-- | /O(N * D)/
toVector :: forall v a. G.Vector v a => Odd a -> v a
toVector = mapToVector id

toList :: Odd a -> [a]
toList Odd{slice#, length#} =
  slice# Lazy (\_ x xs -> x:xs) [] 0# length#

--------------------------------------------------------------------------------
-- Size

length :: Odd a -> Int
length Odd{length#} = I# length#

depth :: Odd a -> Int
depth Odd{depth#} = I# depth#

--------------------------------------------------------------------------------
-- Internal utility

{-# INLINE mapToVector #-}
mapToVector :: forall v b a. G.Vector v b => (a -> b) -> Odd a -> v b
mapToVector f Odd{slice#, length#} = G.create (do
  m <- GM.new (I# length#)
  slice#
    Lazy
    (\i# a -> (>> GM.write m (I# i#) (f a)))
    (return ())
    0#
    length#
  return m)

--------------------------------------------------------------------------------
-- Indexing

{-# INLINE (!) #-}
(!) :: Odd a -> Int -> a
(!) Odd{slice#, length#} (I# i#)
  | tagToEnum# (i# <# length#) = slice# Strict (\_ a _ -> a) undefined i# (i# +# 1#)
  | otherwise                  = outOfBoundsError# "(!)" i# length#

{-# INLINE (!?) #-}
(!?) :: Odd a -> Int -> Maybe a
(!?) Odd{slice#, length#} (I# i#)
  | tagToEnum# (i# <# length#) = slice# Strict (\_ a _ -> Just a) Nothing i# (i# +# 1#)
  | otherwise                  = Nothing

--------------------------------------------------------------------------------
-- Maps

{-# INLINE map #-}
map :: forall v a b. G.Vector v b => (a -> b) -> Odd a -> Odd b
map f o@Odd{length#} = Odd
  { length#
  , depth# = 1#
  , slice# = isfoldr# @v vec
  } where !vec = mapToVector @v f o

--------------------------------------------------------------------------------
-- Folds

{-# INLINE foldr #-}
{-# INLINE ifoldr #-}
{-# INLINE foldl #-}
{-# INLINE ifoldl #-}
{-# INLINE foldl' #-}
{-# INLINE ifoldl' #-}

-- | Right-associative fold
foldr :: (a -> b -> b) -> b -> Odd a -> b
foldr f z Odd{slice#, length#} = slice# Lazy (\_ !x xs -> f x xs) z 0# length#

-- | Right-associative strict fold
foldr' :: (a -> b -> b) -> b -> Odd a -> b
foldr' f z Odd{slice#, length#} = slice# Strict (\_ !x !xs -> f x xs) z 0# length#

-- | Right-associative strict fold, with index
ifoldr :: (Int -> a -> b -> b) -> b -> Odd a -> b
ifoldr f z Odd{slice#, length#} = slice# Lazy (\i !x xs -> f (I# i) x xs) z 0# length#

-- | Right-associative strict fold, with index
ifoldr' :: (Int -> a -> b -> b) -> b -> Odd a -> b
ifoldr' f z Odd{slice#, length#} = slice# Strict (\i !x !xs -> f (I# i) x xs) z 0# length#

-- | Left-associative fold
foldl :: (b -> a -> b) -> b -> Odd a -> b
foldl f z0 Odd{slice#, length#} = slice# Lazy (\_ !x k z -> k (f z x)) id 0# length# z0

-- | Strict left-associative fold
foldl' :: (b -> a -> b) -> b -> Odd a -> b
foldl' f z0 Odd{slice#, length#} = slice# Strict (\_ !x k z -> k $! f z x) id 0# length# z0

-- | Left-associative fold, with index
ifoldl :: (Int -> b -> a -> b) -> b -> Odd a -> b
ifoldl f z0 Odd{slice#, length#} = slice# Lazy (\i !x k z -> k (f (I# i) z x)) id 0# length# z0

-- | Strict left-associative fold, with index
ifoldl' :: (Int -> b -> a -> b) -> b -> Odd a -> b
ifoldl' f z0 Odd{slice#, length#} = slice# Strict (\i !x k z -> k $! f (I# i) z x) id 0# length# z0

instance Foldable Odd where
  foldr = foldr
  foldr' = foldr'
  foldl = foldl
  foldl' = foldl'

--------------------------------------------------------------------------------
-- Traversals

{-# INLINE mapM #-}
{-# INLINE imapM #-}
{-# INLINE mapM_ #-}
{-# INLINE imapM_ #-}

mapM :: Monad m => (a -> m b) -> Odd a -> m (Odd b)
mapM f = fmap fromVector . V.mapM f . toVector

imapM :: Monad m => (Int -> a -> m b) -> Odd a -> m (Odd b)
imapM f = fmap fromVector . V.imapM f . toVector

mapM_ :: Monad m => (a -> m ()) -> Odd a -> m ()
mapM_ f Odd{slice#, length#} =
  slice# Lazy (\i x xs -> xs >> f x) (return ()) 0# length#

imapM_ :: Monad m => (Int -> a -> m ()) -> Odd a -> m ()
imapM_ f Odd{slice#, length#} =
  slice# Lazy (\i x xs -> xs >> f (I# i) x) (return ()) 0# length#

