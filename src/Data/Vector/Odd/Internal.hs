{-# LANGUAGE MagicHash           #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Data.Vector.Odd.Internal where
import qualified Data.Vector          as V
import qualified Data.Vector.Generic  as G
import qualified Data.Vector.Storable as S
import qualified Data.Vector.Unboxed  as U
import           GHC.Int              (Int (..))
import           GHC.Prim             (Int#, tagToEnum#, (+#), (-#), (<#))

type U = U.Vector
type S = S.Vector
type B = V.Vector

--------------------------------------------------------------------------------
-- Utilities

outOfBoundsError :: String -> (Int, Int) -> a
outOfBoundsError a s = error ("Data.Vector." ++ a ++ ": out of bounds " ++ show s)

{-# NOINLINE outOfBoundsError# #-}
outOfBoundsError# :: String -> Int# -> Int# -> a
outOfBoundsError# a len# i# = outOfBoundsError a (I# i#, I# len#)

data FoldType
  = Lazy -- ^ lazy, eagerly producing stream, i.e. foldr
  | Strict -- ^ strict, i.e. foldl'

{-# INLINE isfoldr# #-}
isfoldr#
  :: G.Vector v a
  => v a
  -> FoldType
  -> (Int# -> a -> b -> b)
  -> b
  -> Int#
  -> Int#
  -> b
isfoldr# v = \m -> case m of
  Lazy -> \f z start end ->
    let
      go i = case i <# end of
        1# -> f i (G.unsafeIndex v (I# i)) (go (i +# 1#))
        _  -> z
    in
      go start

  Strict -> \f z start end ->
    let
      go i acc = case i <# end of
        1# -> go (i +# 1#) $! f i (G.unsafeIndex v (I# i)) acc
        _  -> acc
    in
      go start $! z

{-# INLINE max# #-}
{-# INLINE min# #-}
max# :: Int# -> Int# -> Int#
max# a b = if tagToEnum# (a <# b) then a else b

min# :: Int# -> Int# -> Int#
min# a b = if tagToEnum# (a <# b) then b else a

