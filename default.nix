{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc863" }:
let haskell = nixpkgs.haskell; in
haskell.lib.doBenchmark
  (haskell.packages.${compiler}.callCabal2nix "vector-odd" ./. {})

